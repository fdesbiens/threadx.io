---
title: "Eclipse ThreadX"
date: 2023-11-16T10:00:00-04:00
headline: Azure RTOS is now <br>Eclipse ThreadX
subtitle: The First Open Source RTOS <br>Certified for Safety-Critical Applications
links: [
  [href: 'https://accounts.eclipse.org/mailing-list/threadx', text: 'Get Connected'],
  [href: 'https://threadxalliance.org', text: 'Join the ThreadX Alliance']
]
jumbotron_btn_class: btn btn-pill
hide_page_title: true
hide_breadcrumb: true
hide_sidebar: true
page_css_file: /public/css/home.css
layout: single
---

{{< grid/div class="featured-section padding-y-60" isMarkdown="true" >}}

## NEW! Join the ThreadX Alliance
On October 8, 2024, the Eclipse launched the ThreadX Alliance, a global initiative by the Eclipse Foundation to sustain and promote the Eclipse ThreadX real-time operating system (RTOS) and its ecosystem. [See the press release here.](https://www.globenewswire.com/news-release/2024/10/08/2959504/0/en/The-Eclipse-Foundation-Launches-ThreadX-Alliance-to-Champion-the-Growth-and-Sustainability-of-the-World-s-First-and-Only-Safety-Certified-Open-Source-RTOS.html)

The ThreadX Alliance offers its participants the possibility of licensing the ThreadX safety documentation package. Visit [threadxalliance.org](https://threadxalliance.org) for more details.

## Eclipse ThreadX: A New Era for Embedded RTOS Technology

Microsoft has contributed the Azure RTOS technology to the Eclipse Foundation.
With the Eclipse Foundation as its new home, Azure RTOS now becomes Eclipse
ThreadX – an advanced embedded development suite including a small but powerful
operating system that provides reliable, ultra-fast performance for
resource-constrained devices.

Eclipse ThreadX offers a vendor-neutral, open source, safety certified OS for
real-time applications, all under a permissive license. It stands alone as the
first and only RTOS with this unique blend of attributes to meet a wide range
of needs that will benefit industry adopters, developers and end users alike.

This transformative collaboration paves the way for unprecedented advancements
in embedded RTOS technology and will unfold within the framework of the 
[Eclipse ThreadX Project](https://projects.eclipse.org/proposals/eclipse-threadx)
and the [Eclipse ThreadX Special Interest Group](https://sdv.eclipse.org/special-interest-groups/threadx/).

Join us in shaping the future of embedded systems and fostering innovation with
the power of open source!

{{</ grid/div >}}

{{< pages/home/resources >}}

