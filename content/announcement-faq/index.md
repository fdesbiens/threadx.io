---
title: "Annoucemement: Frequently Asked Questions"
date: 2023-11-16T10:00:00-04:00
headline: "Annoucemement: Frequently Asked Questions"
hide_sidebar: true
hide_page_title: true
---

Last Updated: April 30, 2024

## Table of contents

- [What is being announced?](#what-is-being-announced)
- [What is an RTOS?](#what-is-an-rtos)
- [What Azure RTOS components/technologies are being contributed to the Eclipse Foundation?](#what-azure-rtos-componentstechnologies-are-being-contributed-to-the-eclipse-foundation)
- [What is the new name for this technology going forward?](#what-is-the-new-name-for-this-technology-going-forward)
- [Who are the stakeholders and participants in this initiative?](#who-are-the-stakeholders-and-participants-in-this-initiative)
- [How will this contribution impact the open source community and the embedded RTOS industry as a whole?](#how-will-this-contribution-impact-the-open-source-community-and-the-embedded-rtos-industry-as-a-whole)
- [How will ThreadX licensing terms and conditions be impacted?](#how-will-threadx-licensing-terms-and-conditions-be-impacted)
- [What happens with the existing safety and security certifications for ThreadX?](#what-happens-with-the-existing-safety-and-security-certifications-for-threadx)
- [What does safety certified OS mean?](#what-does-safety-certified-os-mean)
- [How does ThreadX fit into the broader Embedded, IoT and Edge Computing ecosystem at the Eclipse Foundation?](#how-does-threadx-fit-into-the-broader-embedded-iot-and-edge-computing-ecosystem-at-the-eclipse-foundation)
- [The Eclipse Foundation already has an embedded operating system project named Oniro. What does the ThreadX announcement mean for Oniro?](#the-eclipse-foundation-already-has-an-embedded-operating-system-project-named-oniro-what-does-the-threadx-announcement-mean-for-oniro)
- [How will the Eclipse Foundation and its community manage, maintain and support TheadX going forward?](#how-will-the-eclipse-foundation-and-its-community-manage-maintain-and-support-theadx-going-forward)
- [In this new scenario, what are some of the advantages of using ThreadX over other embedded RTOS options?](#in-this-new-scenario-what-are-some-of-the-advantages-of-using-threadx-over-other-embedded-rtos-options)
- [Are there any specific use cases or applications the foundation envisions for ThreadX?](#are-there-any-specific-use-cases-or-applications-the-foundation-envisions-for-threadx)
- [How can developers and organizations get involved with or utilize the newly open sourced ThreadX technology?](#how-can-developers-and-organizations-get-involved-with-or-utilize-the-newly-open-sourced-threadx-technology)

---

### What is being announced?

Microsoft is contributing the Azure RTOS technology to the Eclipse Foundation under the Eclipse ThreadX open source project. Eclipse ThreadX will be the world’s first permissively licensed open source embedded real-time operating system (RTOS) with multiple safety and security certifications governed under the stewardship of a vendor-neutral foundation.

---

### What is an RTOS?

An RTOS, or Real-Time Operating System, is tailored to meet the distinctive needs of real-time systems spanning various domains, including consumer electronics, embedded systems, Internet of Things (IoT) devices, industrial control systems, automotive systems, avionics, medical devices, and more. An RTOS is characterized by its deterministic behavior, ensuring predictable and consistent task execution times. Efficient task scheduling, low-latency responses to external events, and robust interrupt handling are fundamental features that distinguish an RTOS. Additionally, effective communication and synchronization mechanisms, along with resource management, contribute to the reliability and precision required in real-time systems.

---

### What Azure RTOS components/technologies are being contributed to the Eclipse Foundation?

- ThreadX - an advanced real-time operating system (RTOS) designed specifically for deeply embedded applications
- NetX Duo - an advanced, industrial-grade TCP/IP network stack designed specifically for deeply embedded real-time and IoT applications. It also includes a MQTT client.
- FileX - a high-performance, FAT-compatible file system that’s fully integrated with ThreadX kernel
- GUIX - a complete, embedded graphical user interface (GUI) library
- USBX - a high-performance USB host, device, and on-the-go (OTG) embedded stack, that is fully integrated with ThreadX kernel
- LevelX - Flash Wear Leveling for FileX and stand-alone purposes
- GuiX Studio - a design environment facilitating the creation and maintenance of all graphical elements for CUIX
- TraceX - an analysis tool that provides a graphical view of real-time system events to better understand the behavior of real-time systems

---

### What is the new name for this technology going forward?

“Azure RTOS” is a Microsoft brand, which cannot be transferred to the Eclipse Foundation. Therefore, going forward the brand will be “Eclipse ThreadX”, or “ThreadX” in its short form. This represents a return to the roots of this technology and clarifies that it will continue.

---

### Who are the stakeholders and participants in this initiative?

Microsoft is contributing the technology to the Eclipse Foundation and will provide the development resources for the first release of Eclipse ThreadX under the Eclipse Foundation Development Process.

Several organizations, including the core semiconductor manufacturers providing ThreadX-enabled products will be establishing an Eclipse Foundation industry collaboration to support the continued evolution and adoption of ThreadX.
[AMD](https://www.amd.com), [Arm](https://www.arm.com), [Cypherbridge](https://www.cypherbridge.com), [Ericsson](https://www.ericsson.com/), [Microsoft](https://www.microsoft.com/), [NXP](https://www.nxp.com/), [PX5](https://px5rtos.com/), [Renesas](https://www.renesas.com/), [Silicon Labs](https://www.silabs.com/), [ST Microelectronics](https://www.st.com/), and [Witekio (an Avnet company)](https://witekio.com/) have all committed to supporting this effort.
Their leadership will be instrumental in driving the direction and future success of Eclipse ThreadX. All interested parties in the ThreadX ecosystem are encouraged to join and participate in this industry collaboration.

---

### How will this contribution impact the open source community and the embedded RTOS industry as a whole?

Eclipse ThreadX will be the world’s first open source real time operating system (RTOS) with multiple safety and security certifications governed under the stewardship of a vendor-neutral foundation. Eclipse ThreadX provides a vendor-neutral, open source, safety certified OS for real time applications published under a permissive license. Eclipse ThreadX is the first and only RTOS in the market that has all four of those attributes and comes with a complete embedded development suite.

---

### How will ThreadX licensing terms and conditions be impacted?

Eclipse ThreadX will be made available under the MIT license, a well-known and highly regarded permissive open source license. This will make adoption of and contribution to ThreadX as simple as possible for all parties.

---

### What happens with the existing safety and security certifications for ThreadX?

Azure RTOS core components have been certified according to the strict standards of [SGS-TÜV Saar](https://www.sgs-tuev-saar.com/) under the requirement for IEC 61508 SIL4.
The functional safety artifacts device developers need to help certify their devices to the IEC 61508, IEC 62304, ISO 26262, EN 50128 and related functional safety standards will be contributed by Microsoft to the Eclipse Foundation and will continue to be maintained and updated by the Eclipse ThreadX Working Group.

---

### What does safety certified OS mean?

ThreadX includes the safety certifications inherited from Azure RTOS whose core components have been certified according to the strict standards of [SGS-TÜV Saar](https://www.sgs-tuev-saar.com/) under the requirement for IEC 61508 SIL4.
The functional safety artifacts device developers need to help certify their devices to the IEC 61508, IEC 62304, ISO 26262, EN 50128 and related functional safety standards will be contributed by Microsoft to the Eclipse Foundation and will continue to be maintained and updated by the Eclipse ThreadX Working Group.

---

### How does ThreadX fit into the broader Embedded, IoT and Edge Computing ecosystem at the Eclipse Foundation?

ThreadX will become a project under the Eclipse IoT top-level project and the guidance of its Project Management Committee (PMC). As such, the ThreadX open source project is joining a large and well-established open source community.

The Eclipse IoT Working Group and its Edge Native special interest group (SIG) are home to the most extensive open source IoT and edge computing toolkit in the industry. The project portfolio encompasses the entire device-to-edge-to-cloud continuum.This software portfolio targets the three pillars of IoT and Edge: constrained devices, edge nodes, and IoT Cloud platforms.

Eclipse ThreadX reinforces the Eclipse IoT and edge portfolio in a significant way. It brings to the table a mature, high-performance, small footprint real time operating system (RTOS) that other Eclipse projects can leverage. This also opens up the use and adoption of other Eclipse IoT technologies, such as Sparkplug and zenoh, to ThreadX developers.

The Eclipse ThreadX safety and security certifications also make it a very attractive platform for software-defined vehicles (SDV). Eclipse SDV projects targeting the in-car environment will undoubtedly benefit from the addition of ThreadX to the Eclipse portfolio.

---

### The Eclipse Foundation already has an embedded operating system project named Oniro. What does the ThreadX announcement mean for Oniro?

ThreadX is a great opportunity for Oniro. One of the main features of Oniro is the swappable kernels and it is already supporting both Yocto based and LiteOS kernels. ThreadX is another RTOS that could be supported by Oniro. ThreadX and Oniro do not compete but cooperate to bring additional features to IoT devices. Oniro’s layered architecture lends itself to higher-level value added features and applications being deployed on top of the various kernels it supports.

---

### How will the Eclipse Foundation and its community manage, maintain and support TheadX going forward?

The Eclipse Foundation will establish a structure to support the development and adoption of ThreadX. The Foundation’s vendor-neutral model of governance will ensure that all stakeholders of the ThreadX ecosystem can participate and make their voices heard.

---

### In this new scenario, what are some of the advantages of using ThreadX over other embedded RTOS options?

Since it is available under the permissive MIT license, you can use ThreadX without licensing or royalty fees. 

If your product requires safety certification, ThreadX represents a mature, proven, yet affordable alternative to commercial RTOSes.

---

### Are there any specific use cases or applications the foundation envisions for ThreadX?

Eclipse ThreadX is a good choice for any type of embedded, IoT, or industrial automation application where a real-time operating system is needed. The impressive list of ThreadX safety and security certifications (e.g. IEC 61508, IEC 62304, ISO 26262, EN 50128) make it an attractive choice for applications and use cases requiring a high level of dependability.

While the suitability of any product for use in a specific application or use case needs to be determined by the implementer, these certifications are designed to meet the varying needs of a broad range of industry applications:

#### **IEC 61508**: Functional Safety of Electrical/Electronic/Programmable Electronic Safety-Related Systems

**Applications**: IEC 61508 is applicable to multiple industries, including:

- Chemical and processing
- Oil and gas
- Power generation and distribution
- Rail transportation
- Automotive
- Aerospace

#### **IEC 62304**: Medical Device Software - Software Life Cycle Processes

**Applications**: IEC 62304 is specifically designed for the software development life cycle of medical device software. It is applicable to:

- Healthcare and medical devices
- Diagnostic equipment
- Monitoring devices
- Therapeutic devices
- Health information technology

#### **ISO 26262**: Road Vehicles - Functional Safety

**Applications**: ISO 26262 is tailored for the automotive industry:

- Automotive electronic systems
- Advanced driver-assistance systems (ADAS)
- Engine control systems
- Electric and hybrid vehicles
- Autonomous vehicles

#### **EN 50128**: Railway Applications - Communications, Signaling, and Processing Systems

**Applications**: EN 50128 is specific to the railway industry:

- Train control and management systems
- Signaling systems
- Communication systems in railways
- Processing systems used in railways

---

### How can developers and organizations get involved with or utilize the newly open sourced ThreadX technology?

Like other Eclipse Foundation projects, Eclipse ThreadX is open, transparent, and meritocratic. The GitHub repositories are public. Anyone can contribute changes to the code or submit issues. Naturally, the permissive MIT license used means adopters can freely use the technology in their products.

The Eclipse ThreadX project and ThreadX interest group each have a public mailing list. Other communication channels will also be made available to interact with the committers and other stakeholders of the Eclipse ThreadX community. Here are some links to these resources:

- [Contact us](mailto:threadx-info@eclipse.org) about joining the [Eclipse ThreadX Interest Group](https://projects.eclipse.org/interest-groups/threadx-interest-group)
- Contribute to the [project](https://projects.eclipse.org/projects/iot/iot.threadx) and join the [Eclipse ThreadX developer mailing list](https://accounts.eclipse.org/mailing-list/threadx-dev)
- Stay connected by joining the general [Eclipse ThreadX mailing list](https://accounts.eclipse.org/mailing-list/threadx)

---
