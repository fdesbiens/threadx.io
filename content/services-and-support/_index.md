---
title: "Services and Support"
seo_title: "Eclipse ThreadX Services and Support"
description: "Services and Support"
keywords: ["Threadx", "eclipse ThreadX", "services", "support"]
toc: false
draft: false
hide_sidebar: true
layout: "single"
---

The Eclipse Foundation is a vendor-neutral, non-profit organisation hosting open-source projects and open specifications. Foundation staff supports the [committers](https://projects.eclipse.org/projects/iot.threadx/who) and contributors of the Eclipse ThreadX open source project but does not directly work on the ThreadX codebase. 

The organisations featured on this page are members of the Eclipse Foundation who offer support plans and other services related to ThreadX. They are featured in alphabetical order. Inclusion on this page does not represent an endorsement by the Eclipse Foundation AISBL and its affiliates of the products or services offered.

{{< members_details >}}